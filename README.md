# amqp-node

Tutorials from <https://www.rabbitmq.com/getstarted.html>.

Index:

1. Send/Receive

Basic syntax on sending and recieving messages to queue. Covers connecting to the server, creating channels, sending and receiving messages. 

2. Worker Queues

Distribute tasks among multiple workers. By default they distribute them in a round robin fashion.
Workers can also have a prefetch value. This is the maximum amount of task a worker can have at a given time. 

3. Publish/Subscribe

We are creating a logging system where a publisher will publish logs and consumers (multiple) can receive and print them. 

The relationship between exchange and a queue is called a binding

4. Routing

This exercise demonstrate how we can deliver messages from exchange to multiple queues based on routing key. 
We extend our logging demo. We add a routing key in the message and exchange forwards it to queues subscribed to it. 

5. Topics 



6. RPC

